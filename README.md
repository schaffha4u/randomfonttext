# RandomFontText

## Bienvenue sur mon site!

**Un site qui génère un texte avec des polices de caractères aléatoires** .

_Tape du texte, et observe le résultat !_

Techo utilisée : HTML, CSS, VanillaJS

Le site est disponible [ici](https://schaffha4u.gitlab.io/randomfonttext/).