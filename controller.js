/****************************/
function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
  
  function getRandomFont(){
    let fonts = ['28Days', 'AlexBrush', 'AllTheRoll', 'BarierSignature', 'Baroneys',
                'Bellyn', 'CaliforniaSun', 'Coneria', 'Dunkelheit', 'EbertaOutline',
              'EbertaRegular', 'EbertaScript', 'Fifth-Grader', 'Finder',
            'Glycolysis-Regular', ];
    let rand = fonts[Math.floor(Math.random() * fonts.length)];

    return rand;
  }

/* Un conteneur contentEditable remets par défaut le curseur au début du conteneur
   Pour éviter ce problème, on utilise cette fonction et on mets le curseur à la fin
 */
function setCaretToEnd(target/*: HTMLDivElement*/) {
    const range = document.createRange();
    const sel = window.getSelection();
    range.selectNodeContents(target);
    range.collapse(false);
    sel.removeAllRanges();
    sel.addRange(range);
    target.focus();
    range.detach(); // optimization

    // set scroll to the end if multiline
    target.scrollTop = target.scrollHeight;
}

/*
Rafraîchit la div et changer le font de chaque lettre
 */
function refreshAll(){
    $(document).ready(function() {
        $("#floatContainer").lettering();
    });

    setCaretToEnd(document.getElementById("floatContainer"));

    let listChilds = document.getElementById("floatContainer").childNodes;

    listChilds.forEach((lettre) => {
        lettre.style.fontFamily = getRandomFont();
    })
}

/*
Rafraichit uniquement le font de la dernière lettre tapée
Ici, obligé de stocker les fonts de chaque lettre dans tabFonts avant de refaire un lettering
 car le lettering va remettre à zéro toutes les fonts des <span>
 */
function refreshLast(){

    $(document).ready(function() {
        $("#floatContainer").lettering();
    });

    setCaretToEnd(document.getElementById("floatContainer"));

}

window.onload = $(function() {//Appel à la création de la fenêtre
    document.getElementById("floatContainer").onkeypress = function(){//Ajout d'un listener
       refreshAll();
    };
})

var i = 0; //Déclaration globale pour l'appel récursif de loadFunction, qui va itérer dans s;
var s = "Bienvenue.";

function loadFunction(){//Se charge pour mettre en forme le texte au moment du load, sans rentrer dans le listener de la div
    var div = document.getElementById("floatContainer");
    if (i < s.length){
        div.innerHTML += s.charAt(i);
        i++;
        refreshAll();
        setTimeout(loadFunction,   200  );
    }
}