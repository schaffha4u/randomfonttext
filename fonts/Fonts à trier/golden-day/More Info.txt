If you want the full version and license for commercial use,
you can purchase here :

https://displaytypefont.com/product/golden-day/

File Font Version : 

1. Golden Day.otf
2. Golden Day.ttf
3. Golden Day.woff
4. Golden Day Italic.otf
5. Golden Day Italic.ttf
6. Golden Day Italic.woff
7. Golden Day Left.otf
8. Golden Day Left.ttf
9. Golden Day Left.woff
